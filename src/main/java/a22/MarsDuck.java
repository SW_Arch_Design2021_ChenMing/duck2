package a22;

public class MarsDuck extends Duck {
    public MarsDuck(){
        flyBehavior = new FlyNoWay();
    }

    @Override
    public void display(){
        System.out.println("This is a Mars duck.");
    }
}
