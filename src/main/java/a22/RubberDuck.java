package a22;

public class RubberDuck extends Duck {
    public RubberDuck(){
        flyBehavior = new FlyNoWay();
    }

    @Override
    public void display(){
        System.out.println("This is a rubber duck.");
    }
}
