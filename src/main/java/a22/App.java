package a22;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Duck duck = new RedHeadDuck();
        printout(1, duck);

        Duck duck2 = new MallardDuck();
        printout(2, duck2);

        Duck duck3 = new RubberDuck();
        printout(3, duck3);

        Duck duck4 = new MarsDuck();
        printout(4, duck4);
        duck4.setFlyMode(new RocketFly());
        printout(4, duck4);
    }

    private static void printout(int ndx, Duck duck){
        System.out.println( "....." + ndx + " duck created" );
        duck.display();
        duck.performFly();
    }
}
